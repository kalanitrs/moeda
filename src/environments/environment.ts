// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDuP_obEdKXDY_odii26WFIyK1VAHa1pAI",
    authDomain: "moeda-27537.firebaseapp.com",
    databaseURL: "https://moeda-27537.firebaseio.com",
    projectId: "moeda-27537",
    storageBucket: "moeda-27537.appspot.com",
    messagingSenderId: "746182975928"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
