import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, private db:AngularFireDatabase) {
    this.user = firebaseAuth.authState;
   }

  register(email: string, password: string) {
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  updateProfile(user, name:string) {
    user.updateProfile({displayName: name, photoURL: ''});
  }

  addUser(user, name:string, nickname:string) {
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).child('details').push({'name': name});
    ref.child('users').child(uid).child('details').push({'nickname': nickname});
  }

  login(email: string, password: string) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email,password);
  }

  logOut() {
    return this.firebaseAuth.auth.signOut();
  }
}
