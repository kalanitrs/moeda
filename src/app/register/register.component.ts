import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import {Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  name: string;
  nickname: string;
  required = "";
  passmessage = "";
  repassword:string;

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  register() {
    if (this.email == null || this.name == null || this.nickname == null || this.password == null) {
      this.required = "You should fill this field";
      return
    }
    if (this.password.length < 6) {
      this.passmessage = "password is too short";
      return
    }
    if (this.password != this.repassword) {
      this.passmessage = "passwords are not identical";
      return
    }
    this.authService.register(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name)
      this.authService.addUser(value.user, this.name, this.nickname)
    }).then(value => {
      this.router.navigate(['']);
    });
  }

}
