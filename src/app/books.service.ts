import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private authService:AuthService, private db: AngularFireDatabase) { }

  addBook(name: string, author: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/books').push({'name':name,'author':author});
      }
    )
  }
}
