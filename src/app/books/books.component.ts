import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  nickname: string;
  userdata = [];
  name: string;
  author: string;

  constructor(private authService: AuthService, private db:AngularFireDatabase, private booksService:BooksService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        if (!user) return;
        this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
          data => {
            data.forEach(
              dat => {
                let y = dat.payload.toJSON();
                this.userdata.push(y);                
              }
            )
            this.nickname = this.userdata[1].nickname;
          }
        )
      }
    )
  }

  addBook() {
    this.booksService.addBook(this.name, this.author);
    this.name = '';
    this.author = '';
  }

}
